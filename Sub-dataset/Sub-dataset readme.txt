Arabic Health Services Twitter Sub-Dataset
30 November 2016
Copyright (C) Abdulaziz M. Alayba
Contact: Abdulaziz M. Alayba (alaybaa@uni.coventry.ac.uk / a.alayba@uoh.edu.sa)


***************************************
General Description
***************************************

The Dataset was collected from Twitter and more details can be found in the paper "Improving Sentiment Analysis in Arabic Using Word Representation".
See the paper https://arxiv.org/abs/1803.00124




***************************************
File Format
***************************************

The files are pos.txt and neg.text.

pos.txt contains all the positive reviews which are 502 reviews  
neg.txt contains all the negative reviews which are 1732 reviews  


***************************************
More Information
***************************************

Please cite the paper "Improving Sentiment Analysis in Arabic Using Word Representation" if you use the Sub-dataset. The citation details are below


A. M. Alayba, V. Palade, M. England and R. Iqbal, "Improving Sentiment Analysis in Arabic Using Word Representation," arXiv:1803.00124 [cs.CL], Feb. 2018.

URL:  https://arxiv.org/abs/1803.00124
