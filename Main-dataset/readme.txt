Arabic Health Services Twitter Main Dataset
28 February 2016
Copyright (C) Abdulaziz M. Alayba
Contact: Abdulaziz M. Alayba (alaybaa@uni.coventry.ac.uk / a.alayba@uoh.edu.sa)


***************************************
General Description
***************************************

The Dataset was collected from Twitter and more details can be found in the paper "Arabic Language Sentiment Analysis on Health Services".
See the paper DOI: 10.1109/ASAR.2017.8067771



***************************************
File Format
***************************************

The files are pos.txt and neg.text.

pos.txt contains all the positive reviews which are 628 reviews  
neg.txt contains all the negative reviews which are 1398 reviews  


***************************************
More Information
***************************************

Please cite the paper "Arabic Language Sentiment Analysis on Health Services" if you use the Main dataset. The citation details are below


A. M. Alayba, V. Palade, M. England and R. Iqbal, "Arabic language sentiment analysis on health services," 2017 1st International Workshop on Arabic Script Analysis and Recognition (ASAR), Nancy, 2017, pp. 114-118.
doi: 10.1109/ASAR.2017.8067771
keywords: {Bayes methods;Big Data;data mining;learning (artificial intelligence);neural nets;pattern classification;regression analysis;sentiment analysis;social networking (online);support vector machines;Arabic language dataset;Arabic language sentiment analysis;Arabic text annotation;Arabic text filtering;Arabic text preprocessing;Convolutional Neural Networks;Twitter;big sentiment analysis dataset;deep neural network;health dataset;health services;logistic regression algorithm;machine learning algorithms;naïve Bayes algorithm;social media network phenomenon;support vector machine algorithm;tweets;Feature extraction;Filtering;Neural networks;Semantics;Sentiment analysis;Support vector machines;Twitter;Arabic Language;Deep Neural Networks;Machine Learning;Sentiment Analysis},
URL: http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8067771&isnumber=8067744



